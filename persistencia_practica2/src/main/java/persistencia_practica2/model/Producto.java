package persistencia_practica2.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "Producto")
@Table(name = "producto")
public class Producto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos",strategy="native")
	private Long id;
	
	private String codigo;
	private String descripcion;
	
	
	@OneToOne
	@JoinColumn(name = "precio_id", foreignKey=@ForeignKey(name="precio_id_fk"))
	private Precio precio;
	
	@OneToOne
	@JoinColumn(name = "precio_historico_id", foreignKey=@ForeignKey(name="precio_historico_id_fk"))
	private Precio precioHistorico;
	
	@ManyToMany(mappedBy="productos")
	private List<Proveedor> proveedores;
	
	public Producto(String codigo, String descripcion, Precio precio, List<Proveedor> proveedores) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.proveedores = proveedores;
	}
	
	public Producto() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Precio getPrecio() {
		return precio;
	}

	public void setPrecio(Precio precio) {
		this.precio = precio;
	}

	public List<Proveedor> getProveedores() {
		return proveedores;
	}

	public void setProveedores(List<Proveedor> proveedores) {
		this.proveedores = proveedores;
	}

	public Precio getPrecioHistorico() {
		return precioHistorico;
	}

	public void setPrecioHistorico(Precio precio) {
		this.precioHistorico = precio;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {return true;}
		if(!(obj instanceof Producto)) {
				return false;
		}
		Producto otherObj = (Producto) obj;
		return this.getId() == otherObj.getId();
	}

}
