package persistencia_practica2.model;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "Detalle")
@Table(name = "detalle")
public class Detalle {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos",strategy="native")
	private Long id;
	
    @ManyToOne
	@JoinColumn(name="factura_id", foreignKey=@ForeignKey(name="factura_id_fk"))
	private Factura factura;
	
	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "producto_id")
	private Producto producto;
	
	private Integer cantidad;
	
	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "precio_id")
	private Precio precio;
	
	public Detalle(Factura factura, Producto producto, Integer cantidad) {
		super();
		this.factura = factura;
		this.producto = producto;
		this.cantidad = cantidad;
	}

	public Detalle() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Precio getPrecio() {
		return precio;
	}

	public void setPrecio(Precio precio) {
		this.precio = precio;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {return true;}
		if(!(obj instanceof Detalle)) {
				return false;
		}
		Detalle otherObj = (Detalle) obj;
		return this.getId() == otherObj.getId();
	}
	
}
