package persistencia_practica2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "Cuenta")
@Table(name = "cuenta")
public class Cuenta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos",strategy="native")
	private Long id;
	
	private String numero;
	
	@OneToOne
	@JoinColumn(name="cliente_id", foreignKey=@ForeignKey(name="cuenta_id_fk"))
	private Cliente cliente;
	

	public Cuenta(String numero, Cliente cliente) {
		super();
		this.numero = numero;
		this.cliente = cliente;
	}
	
	public Cuenta() {
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {return true;}
		if(!(obj instanceof Cuenta)) {
				return false;
		}
		Cuenta otherObj = (Cuenta) obj;
		return this.getId() == otherObj.getId();
	}

}
