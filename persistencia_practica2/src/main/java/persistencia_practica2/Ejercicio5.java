package persistencia_practica2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import persistencia_practica2.model.Cliente;
import persistencia_practica2.model.Cuenta;
import persistencia_practica2.model.Detalle;
import persistencia_practica2.model.Factura;
import persistencia_practica2.model.Precio;
import persistencia_practica2.model.Producto;
import persistencia_practica2.model.Proveedor;

public class Ejercicio5 {

	public static void main(String[] args) {
	    SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		Cliente cliente = new Cliente("ABC43", "Rodriguez", "Pepe");
		
		Cuenta cuenta = new Cuenta("200", cliente);
		
		Factura factura = new Factura(cliente, new Date(10052022), 1);
		
		Producto producto = new Producto("123","Leche");
		
		List<Producto> productos = new ArrayList<Producto>();
		productos.add(producto);
		
		Proveedor proveedor = new Proveedor("Lechero", productos);
		
		Precio precio = new Precio(new BigDecimal(120), new Date(10052022), producto);
		
		Detalle detalle = new Detalle(factura, producto, 20);
		
		try {
			session.beginTransaction();
			session.persist(cliente);
			session.persist(cuenta);
			session.persist(factura);
			session.persist(producto);
			session.persist(precio);
			session.persist(proveedor);
			session.persist(detalle);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

	}

}
