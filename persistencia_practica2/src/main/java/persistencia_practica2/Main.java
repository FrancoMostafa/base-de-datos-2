package persistencia_practica2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import persistencia_practica2.model.Cliente;
import persistencia_practica2.model.Cuenta;
import persistencia_practica2.model.Detalle;
import persistencia_practica2.model.Factura;
import persistencia_practica2.model.Precio;
import persistencia_practica2.model.Producto;
import persistencia_practica2.model.Proveedor;

public class Main {

	public static void main(String[] args) {
	    SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		Cliente cliente = crearCliente("ABC43", "Rodriguez", "Pepe");
		
		Cliente cliente2 = crearCliente("JOO32", "Ramirez", "Carlos");
		
		Cliente cliente3 = crearCliente("ABC43", "Erique", "Roman");
		
		Cuenta cuenta = crearCuenta("200",cliente);
		
		Factura factura = crearFactura(cliente, new Date(10052022), 1);
		
		Factura factura2 = crearFactura(cliente2, new Date(10052022), 2);

		Factura factura3 = crearFactura(cliente3, new Date(10052022), 3);
		
		List<Proveedor> proveedores = new ArrayList<Proveedor>();
		
		List<Proveedor> proveedores2 = new ArrayList<Proveedor>();
				
		Proveedor proveedor = crearProveedor("Proveedor de la serenisima");
		
		Proveedor proveedor2 = crearProveedor("Proveedor de arcor");
		
		Proveedor proveedor3 = crearProveedor("Proveedor de arcor 2");
		
		proveedores.add(proveedor);

		proveedores2.add(proveedor2);
		
		proveedores2.add(proveedor3);
		
		Precio precio = crearPrecio(new BigDecimal(120), new Date(10052022));
		
		Precio precio2 = crearPrecio(new BigDecimal(100), new Date(10052022));

		Precio precio3 = crearPrecio(new BigDecimal(150), new Date(10052022));
		
		Producto producto = crearProducto("123","Leche", precio, proveedores);
		
		Producto producto2 = crearProducto("abc","Galletitas rumba", precio2, proveedores2);

		Producto producto3 = crearProducto("jfg","Chocolate arcor", precio3, proveedores2);
		
		Detalle detalle = crearDetalle(factura,producto,20);
		
		Detalle detalle2 = crearDetalle(factura2,producto2,10);

		Detalle detalle3 = crearDetalle(factura3,producto3,12);
		
		try {
			session.beginTransaction();
			session.persist(cliente);
			session.persist(cliente2);
			session.persist(cliente3);
			session.persist(cuenta);
			session.persist(factura);
			session.persist(factura2);
			session.persist(factura3);
			session.persist(precio);
			session.persist(precio2);
			session.persist(precio3);
			session.persist(producto);
			session.persist(producto2);
			session.persist(producto3);
			session.persist(proveedor);
			session.persist(proveedor2);
			session.persist(proveedor3);
			session.persist(detalle);
			session.persist(detalle2);
			session.persist(detalle3);
			Precio precioNuevo = actualizarPrecio(new BigDecimal(150), new Date(15052022), producto);
			session.persist(precioNuevo);
			session.update(producto);
			session.getTransaction().commit();
			
			session.update(producto);
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

	}
	
	public static Cliente crearCliente(String codigo, String apellido, String nombre) {
		return new Cliente(codigo, apellido, nombre);
	}
	
	public static Cuenta crearCuenta(String numero, Cliente cliente) {
		return new Cuenta(numero, cliente);
	}
	
	public static Detalle crearDetalle(Factura factura, Producto producto, Integer cantidad) {
		return new Detalle(factura,producto,cantidad);
	}
	
	public static Factura crearFactura(Cliente cliente, Date fecha, Integer numero) {
		return new Factura(cliente,fecha,numero);
	}
	
	public static Precio crearPrecio(BigDecimal monto, Date fecha) {
		return new Precio(monto, fecha);
	}
	
	public static Producto crearProducto(String codigo, String descripcion, Precio precio, List<Proveedor> proveedor) {
		return new Producto(codigo,descripcion, precio, proveedor);
	}
	
	public static Proveedor crearProveedor(String descripcion) {
		return new Proveedor(descripcion);
	}
	
	public static Precio actualizarPrecio(BigDecimal monto, Date fecha, Producto producto) {
		Precio precio = crearPrecio(monto,fecha);
		producto.setPrecioHistorico(producto.getPrecio());
		producto.setPrecio(precio);
		return precio;
	}

}
