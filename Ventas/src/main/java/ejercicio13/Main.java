package ejercicio13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;

import modelo.*;

public class Main {

		private static Connection dbConnection = null;

		public static void main(String[] args) {
			try {
				
				@SuppressWarnings("unused")
				SimpleDateFormat sdf = new SimpleDateFormat(
					    "MM-dd-yyyy");
					int year = 2022;
					int month = 04;
					int day = 12;
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.YEAR, year);
					cal.set(Calendar.MONTH, month - 1); // <-- months start
					                                    // at 0.
					cal.set(Calendar.DAY_OF_MONTH, day);

					java.sql.Date date = new java.sql.Date(cal.getTimeInMillis());
				
				Precio precio = new Precio(4, 100.00, date, 1);
				
				Class.forName("com.mysql.cj.jdbc.Driver");
				
				dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cliente?serverTimezone=UTC&autoReconnect=true&useSSL=false", "Franco", "franko393939");
				String query1 ="insert into precio (id_precio, monto, fecha, id_producto) values (?, ?, ?, ?)";
		        PreparedStatement preparedStatement;
		        preparedStatement = dbConnection.prepareStatement(query1);
		        preparedStatement.setInt(1, precio.getId_precio());
		        preparedStatement.setDouble(2, precio.getMonto());
		        preparedStatement.setDate(3, precio.getFecha());
		        preparedStatement.setInt(4, precio.getId_producto());
		        preparedStatement.executeUpdate();
		        preparedStatement.close();
				String query2 ="update producto set id_precio = ? where id_producto = ?";
		        preparedStatement = dbConnection.prepareStatement(query2);
		        preparedStatement.setInt(1, precio.getId_precio());
		        preparedStatement.setInt(2, precio.getId_producto()); 
		        preparedStatement.executeUpdate();
		        } catch (ClassNotFoundException e) {
				System.out.println(e.getMessage());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			
		}

	}
