package modelo;

import java.sql.Date;

public class Precio {
	
	private int id_precio;
	private double monto;
	private Date fecha;
	private int id_producto;
	
	public Precio(int id_precio, double monto, Date fecha, int id_producto) {
		super();
		this.id_precio = id_precio;
		this.monto = monto;
		this.fecha = fecha;
		this.id_producto = id_producto;
	}

	public int getId_precio() {
		return id_precio;
	}

	public void setId_precio(int id_precio) {
		this.id_precio = id_precio;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getId_producto() {
		return id_producto;
	}

	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	

}
