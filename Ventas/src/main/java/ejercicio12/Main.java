package ejercicio12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import modelo.Cliente;

public class Main {

		private static Connection dbConnection = null;

		public static void main(String[] args) {
			try {
				
				Cliente cliente = new Cliente(4, "AFF34", "Rodriguez", "Perez");
				
				Class.forName("com.mysql.cj.jdbc.Driver");
				
				dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cliente?serverTimezone=UTC&autoReconnect=true&useSSL=false", "Franco", "franko393939");
				String query="insert into cliente (id_cliente, codigo, apellido, nombre) values (?, ?, ?, ?)";
		        PreparedStatement preparedStatement;
		        preparedStatement = dbConnection.prepareStatement(query);
		        preparedStatement.setInt(1, cliente.getId_cliente());
		        preparedStatement.setString(2, cliente.getCodigo());
		        preparedStatement.setString(3, cliente.getNombre());
		        preparedStatement.setString(4, cliente.getApellido());
		        preparedStatement.executeUpdate();
		        preparedStatement.close();
		        dbConnection.close();
			} catch (ClassNotFoundException e) {
				System.out.println(e.getMessage());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}		
		}

	}
