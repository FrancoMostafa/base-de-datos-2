package persistencia_practica3;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import net.minidev.json.JSONObject;
import persistencia_practica3.dto.ClienteDTO;
import persistencia_practica3.dto.CuentaDTO;
import persistencia_practica3.dto.DetalleDTO;
import persistencia_practica3.model.CuentaClienteRequest;
import persistencia_practica3.model.General;
import persistencia_practica3.model.Precio;
import persistencia_practica3.model.Producto;
import persistencia_practica3.model.Proveedor;
import persistencia_practica3.repository.PrecioRepository;
import persistencia_practica3.repository.ProductoRepository;
import persistencia_practica3.repository.ProveedorRepository;
import persistencia_practica3.service.ClienteService;
import persistencia_practica3.service.CuentaService;
import persistencia_practica3.service.PrecioService;
import persistencia_practica3.service.ProductoService;
import persistencia_practica3.service.ProveedorService;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
class PersistenciaPractica3ApplicationTests {
	
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ProveedorService proveedorService;

	@Autowired
	private ProductoService productoService;
	
	@Autowired
	private PrecioService precioService;

	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private CuentaService cuentaService;
	
	@Autowired
	private ProveedorRepository proveedorRepository;
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private PrecioRepository precioRepository;

	@Test
	@Rollback(false)
	@Transactional 
	public void ejercicio2() {
		// CREAR PROVEEDOR
		Proveedor proveedor = proveedorService.create(new Proveedor("Proveedor de la serenisima"));
		// COMPROBAR QUE SE CREO EN BASE DE DATOS
		List<Proveedor> proveedores = (List<Proveedor>) proveedorRepository.findAll();
		List<Long> ids = proveedores.stream().map(p -> p.getId()).collect(Collectors.toList());
		assertTrue(ids.contains(proveedor.getId()));
	}
	
	@Test
	@Rollback(false)
	@Transactional 
	public void ejercicio3() {
		// CREAR PRECIO, PRODUCTO Y PROVEEDOR
		Proveedor proveedor = proveedorService.create(new Proveedor("Proveedor de chocolate"));
		Precio precio = precioService.create(new Precio(new BigDecimal("44.0000"), "2022-07-01 15:19"));
		Producto producto = productoService.createGeneral(new General("ABC1","Heladera", precio, 500));
		// AGREGAR PROVEEDOR DE EJERCICIO 2
		Producto productoSave = productoService.agregarProveedor(producto.getId(), proveedor.getId());
		// COMPROBAR EN BASE DE DATOS
		List<Long> idsProveedoresProducto = productoSave.getProveedores().stream().map(p -> p.getId()).collect(Collectors.toList());
		List<Producto> productos = (List<Producto>) productoRepository.findAll();
		List<Long> idsProductos = productos.stream().map(p -> p.getId()).collect(Collectors.toList());
		List<Precio> precios = (List<Precio>) precioRepository.findAll();
		List<Long> idsPrecios = precios.stream().map(p -> p.getId()).collect(Collectors.toList());
		assertTrue(idsProveedoresProducto.contains(proveedor.getId())); // PROVEEDOR CREADO EN EJERCICIO 2
		assertTrue(idsProductos.contains(producto.getId()));
		assertTrue(idsPrecios.contains(precio.getId()));
	}
	
	@Test
	@Rollback(false)
	@Transactional 
	public void ejercicio4() throws Exception {
		// REGISTER DATE
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		// MODELOS
		Precio precio = precioService.create(new Precio(new BigDecimal("44.0000"), "2022-07-01 15:19"));
		Producto producto = productoService.createGeneral(new General("ABC1","Heladera", precio, 500));
		ClienteDTO clienteDTO = clienteService.create(new ClienteDTO("C1", "Perez", "Eduardo"));
		CuentaDTO cuentaDTO = cuentaService.create(new CuentaDTO("1"));
		clienteService.setCuenta(new CuentaClienteRequest(cuentaDTO.getId(), clienteDTO.getId()));
		// LISTA DETALLES
		List<DetalleDTO> detallesDto = new ArrayList<DetalleDTO>();
		detallesDto.add(new DetalleDTO(producto.getId(), 5));
		// FACTURA
		JSONObject json = new JSONObject();
		json.put("idCliente",1);
		json.put("fecha","2022-07-01 15:19");
		json.put("numero",1);
		json.put("detallesDto",detallesDto);
		mockMvc.perform(post("/factura").contentType(MediaType.APPLICATION_JSON).content(json.toString()))
		.andExpect(status().isCreated());
	}
	

}
