package persistencia_practica3.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="Congelado")
@PrimaryKeyJoinColumn(name = "congelado_id")
public class Congelado extends Alimento {

	public Congelado(String codigo, String descripcion, Precio precio,
			LocalDate fechaDeIngreso) {
		super(codigo, descripcion, precio, fechaDeIngreso);
	}

	@Override
	public BigDecimal getPrecioFinal() {
		return this.getPrecio().getMonto().add(this.getPrecio().getMonto().multiply(new BigDecimal("0.08")));

	}

	@Override
	public String toString() {
		return "Congelado [fechaDeIngreso=" + fechaDeIngreso + ", id=" + id + ", codigo=" + codigo + ", descripcion="
				+ descripcion + ", precio=" + precio + ", precioHistorico=" + precioHistorico + ", proveedores="
				+ proveedores + "]";
	}

}