package persistencia_practica3.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="Alimento")
@PrimaryKeyJoinColumn(name = "alimento_id")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Alimento extends Producto {
	
	protected LocalDate fechaDeIngreso;

	public Alimento(String codigo, String descripcion, Precio precio,LocalDate fechaDeIngreso) {
		super(codigo, descripcion, precio);
		this.fechaDeIngreso = fechaDeIngreso;
	}

	public LocalDate getFechaDeIngreso() {
		return fechaDeIngreso;
	}

	public void setFechaDeIngreso(LocalDate fechaDeIngreso) {
		this.fechaDeIngreso = fechaDeIngreso;
	}

	@Override
	public String toString() {
		return "Alimento [fechaDeIngreso=" + fechaDeIngreso + ", id=" + id + ", codigo=" + codigo + ", descripcion="
				+ descripcion + ", precio=" + precio + ", precioHistorico=" + precioHistorico + ", proveedores="
				+ proveedores + "]";
	}

}
