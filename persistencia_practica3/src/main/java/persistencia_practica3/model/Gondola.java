package persistencia_practica3.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="Gondola")
@PrimaryKeyJoinColumn(name = "gondola_id")
public class Gondola extends Alimento {
	
	private Integer volumen;

	public Gondola(String codigo, String descripcion, Precio precio,
			LocalDate fechaDeIngreso, Integer volumen) {
		super(codigo, descripcion, precio, fechaDeIngreso);
		this.setVolumen(volumen);
	}

	@Override
	public BigDecimal getPrecioFinal() {
		if(volumen > 400) {
			return this.getPrecio().getMonto().add(this.getPrecio().getMonto().multiply(new BigDecimal("0.03")));
		}
		else {
			return this.getPrecio().getMonto();
		}
	}

	public Integer getVolumen() {
		return volumen;
	}

	public void setVolumen(Integer volumen) {
		this.volumen = volumen;
	}

	@Override
	public String toString() {
		return "Gondola [volumen=" + volumen + ", fechaDeIngreso=" + fechaDeIngreso + ", id=" + id + ", codigo="
				+ codigo + ", descripcion=" + descripcion + ", precio=" + precio + ", precioHistorico="
				+ precioHistorico + ", proveedores=" + proveedores + "]";
	}

}
