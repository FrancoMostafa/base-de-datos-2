package persistencia_practica3.model;

public class CuentaClienteRequest {
	
	private Long cuentaId;
	private Long clienteId;
	
	public CuentaClienteRequest(Long cuentaId, Long clienteId) {
		super();
		this.cuentaId = cuentaId;
		this.clienteId = clienteId;
	}

	public Long getCuentaId() {
		return cuentaId;
	}

	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}
	
}
