package persistencia_practica3.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "Detalle")
@Table(name = "detalle")
public class Detalle {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos",strategy="native")
	private Long id;
	
	private Long factura;
	
	private Long producto;
	
	private Integer cantidad;
	
	private Long precio;
	
	public Detalle(Integer cantidad) {
		super();
		this.cantidad = cantidad;
	}

	public Detalle() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFactura() {
		return factura;
	}

	public void setFactura(Long factura) {
		this.factura = factura;
	}

	public Long getProducto() {
		return producto;
	}

	public void setProducto(Long producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Long getPrecio() {
		return precio;
	}

	public void setPrecio(Long precio) {
		this.precio = precio;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public String toString() {
		return "Detalle [cantidad=" + cantidad + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {return true;}
		if(!(obj instanceof Detalle)) {
				return false;
		}
		Detalle otherObj = (Detalle) obj;
		return this.getId() == otherObj.getId();
	}
	
}
