package persistencia_practica3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import persistencia_practica3.dto.FacturaDTO;
import persistencia_practica3.service.FacturaService;

@RestController
@RequestMapping("/factura")
public class FacturaController {
	
	// atributos
	@Autowired
	private FacturaService facturaService;

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<FacturaDTO> create(@RequestBody FacturaDTO facturaDTO){
		facturaDTO = this.facturaService.create(facturaDTO);
		return new ResponseEntity<FacturaDTO>(facturaDTO, HttpStatus.CREATED);
	}

}
