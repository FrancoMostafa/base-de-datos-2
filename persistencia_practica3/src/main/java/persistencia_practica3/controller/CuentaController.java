package persistencia_practica3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import persistencia_practica3.dto.CuentaDTO;
import persistencia_practica3.model.CuentaClienteRequest;
import persistencia_practica3.service.CuentaService;

@RestController
@RequestMapping("/cuenta")
public class CuentaController {
	
	// atributos
	@Autowired
	private CuentaService cuentaService;

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<CuentaDTO> create(@RequestBody CuentaDTO cuentaDTO){
		cuentaDTO = this.cuentaService.create(cuentaDTO);
		return new ResponseEntity<CuentaDTO>(cuentaDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/setCliente", method=RequestMethod.PUT)
	public ResponseEntity<CuentaDTO> setCliente(@RequestBody CuentaClienteRequest cuentaClienteRequest){
		var cuentaDTO = this.cuentaService.setCliente(cuentaClienteRequest);
		return new ResponseEntity<CuentaDTO>(cuentaDTO, HttpStatus.OK);
	}
	
}
