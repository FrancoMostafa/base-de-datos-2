package persistencia_practica3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistencia_practica3.model.General;
import persistencia_practica3.model.Producto;
import persistencia_practica3.model.Proveedor;
import persistencia_practica3.repository.ProductoRepository;
import persistencia_practica3.repository.ProveedorRepository;

@Service
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private ProveedorRepository proveedorRepository;
	
	@Override
	public Producto createGeneral(General generalData) {
		General general = new General(generalData.getCodigo(),generalData.getDescripcion(),generalData.getPrecio(), generalData.getPeso()
				);
		this.productoRepository.save(general);
		return general;
	}
	
	@Override
	public Producto agregarProveedor(Long idProducto, Long idProveedor) {
		var proveedores = (List<Proveedor>) proveedorRepository.findAll();
		var productos = (List<Producto>) productoRepository.findAll();
			var proveedor = proveedores.stream().filter(p -> p.getId().equals(idProveedor)).findAny().orElse(null);
			var producto = productos.stream().filter(p -> p.getId().equals(idProducto)).findAny().orElse(null);
			if(proveedor != null && producto != null) {
				producto.agregarProveedor(proveedor);
				proveedor.agregarProducto(producto);
				productoRepository.save(producto);
				proveedorRepository.save(proveedor);
			}
			return producto;
		}

}
