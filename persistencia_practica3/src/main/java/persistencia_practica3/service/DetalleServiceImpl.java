package persistencia_practica3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistencia_practica3.dto.DetalleDTO;
import persistencia_practica3.model.Detalle;
import persistencia_practica3.model.Producto;
import persistencia_practica3.repository.DetalleRepository;
import persistencia_practica3.repository.ProductoRepository;

@Service
public class DetalleServiceImpl implements DetalleService {

	@Autowired
	private DetalleRepository detalleRepository;
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Override
	public DetalleDTO create(DetalleDTO detalleDTO) {
		Detalle detalle = new Detalle(detalleDTO.getCantidad());
		var productos = (List<Producto>) productoRepository.findAll();
		var producto = productos.stream().filter(p -> p.getId().equals(detalleDTO.getIdProducto())).findAny().orElse(null);
		detalle.setProducto(producto.getId());
		detalle.setPrecio(producto.getPrecio().getId());
		detalle.setCantidad(detalleDTO.getCantidad());
		detalleRepository.save(detalle);
		return new DetalleDTO(detalle);
	}

}
