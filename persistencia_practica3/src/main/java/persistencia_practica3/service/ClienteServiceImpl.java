package persistencia_practica3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistencia_practica3.dto.ClienteDTO;
import persistencia_practica3.model.Cliente;
import persistencia_practica3.model.Cuenta;
import persistencia_practica3.model.CuentaClienteRequest;
import persistencia_practica3.repository.ClienteRepository;
import persistencia_practica3.repository.CuentaRepository;

@Service
public class ClienteServiceImpl implements ClienteService{

	//atributos
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private CuentaRepository cuentaRepository;
	
	
//	@Autowired
//	private CuentaRepository cuentaRepository;
	
	public ClienteServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	// metodos
	@Override
	public ClienteDTO create(ClienteDTO clienteDTO) {
		Cliente cli1 = new Cliente(
				clienteDTO.getCodigo(),
				clienteDTO.getApellido(),
				clienteDTO.getNombre());
		this.clienteRepository.save(cli1);
		return new ClienteDTO(cli1);
	}

	@Override
	public ClienteDTO finById(Long idCliente) {
		Cliente cliente = this.clienteRepository.findById(idCliente).orElse(null);
		ClienteDTO clienteDto = null;
		if(cliente !=null) {
			clienteDto = new ClienteDTO(cliente);
		}
		return clienteDto;
	}
	
	@Override
	public List<ClienteDTO> finAll() {
		List<Cliente> clientes = (List<Cliente>) this.clienteRepository.findAll();
		List<ClienteDTO> clientesDto = new ArrayList<ClienteDTO>();
		if(clientes != null) {
			clientes.forEach(c -> clientesDto.add(new ClienteDTO(c)));
		}
		return clientesDto;
	}

	@Override
	public ClienteDTO update(ClienteDTO clienteDTO, Long id) {
		List<Cliente> clientes =  (List<Cliente>) this.clienteRepository.findAll();
		Cliente clienteId = clientes.stream().filter(c -> c.getId().equals(id)).findAny().orElse(null);
		if(clienteDTO == null) {
			return null;
		}
		if(clienteDTO.getApellido() != null) {
		clienteId.setApellido(clienteDTO.getApellido());
		}
		if(clienteDTO.getCodigo() != null) {
		clienteId.setCodigo(clienteDTO.getCodigo());
		}
		if(clienteDTO.getNombre() != null) {
		clienteId.setNombre(clienteDTO.getNombre());
		}
		
		this.clienteRepository.save(clienteId);
		
		return new ClienteDTO(clienteId);
	}

	@Override
	public List<ClienteDTO> delete(Long idCliente) {
		List<Cliente> clientes = (List<Cliente>) this.clienteRepository.findAll();
		Cliente clienteid = clientes.stream().filter(c -> c.getId().equals(idCliente)).findAny().orElse(null);
		clientes.remove(clienteid);
		List<ClienteDTO> clientesDto = new ArrayList<ClienteDTO>();
		if(clientes != null) {
			clientes.forEach(c -> clientesDto.add(new ClienteDTO(c)));

		}
		this.clienteRepository.delete(clienteid);
		return clientesDto;
	}

	@Override
	public ClienteDTO setCuenta(CuentaClienteRequest cuentaClienteRequest) {
		var clientes = (List<Cliente>) this.clienteRepository.findAll();
		var cliente = clientes.stream().filter(c -> c.getId().equals(cuentaClienteRequest.getClienteId())).findAny().orElse(null);
		var cuentas = (List<Cuenta>) this.cuentaRepository.findAll();
		var cuenta = cuentas.stream().filter(c -> c.getId().equals(cuentaClienteRequest.getCuentaId())).findAny().orElse(null);
		if(cuenta == null || cliente == null) {
			return null;
		}
		cliente.setCuenta(cuenta);
		cuenta.setCliente(cliente);
		this.cuentaRepository.save(cuenta);
		this.clienteRepository.save(cliente);
		return new ClienteDTO(cliente);
	}

}
