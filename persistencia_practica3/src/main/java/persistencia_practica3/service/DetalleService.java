package persistencia_practica3.service;

import persistencia_practica3.dto.DetalleDTO;

public interface DetalleService {
	
	public DetalleDTO create(DetalleDTO detalleDTO);

}
