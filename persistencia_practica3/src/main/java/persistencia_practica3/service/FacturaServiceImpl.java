package persistencia_practica3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistencia_practica3.dto.FacturaDTO;
import persistencia_practica3.model.Cliente;
import persistencia_practica3.model.Detalle;
import persistencia_practica3.model.Factura;
import persistencia_practica3.repository.ClienteRepository;
import persistencia_practica3.repository.DetalleRepository;
import persistencia_practica3.repository.FacturaRepository;

@Service
public class FacturaServiceImpl implements FacturaService {
	
	@Autowired
	private FacturaRepository facturaRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
		
	@Autowired
	private DetalleService detalleService;
	
	@Autowired
	private DetalleRepository detalleRepository;

	@Override
	public FacturaDTO create(FacturaDTO facturaDTO) {
		var clientes = (List<Cliente>) clienteRepository.findAll();
		var cliente = clientes.stream().filter(c -> c.getId().equals(facturaDTO.getIdCliente())).findAny().orElse(null);
		Factura fac = new Factura(
				cliente.getId(),
				facturaDTO.getFecha(),
				facturaDTO.getNumero());
		var detalles = facturaDTO.getDetalles();
		for(int i = 0; i < detalles.size(); i++) {
			Detalle detalle = detalleService.create(detalles.get(i)).getDetalle();
			var cantidad = fac.getCantidad();
			fac.setCantidad(cantidad += detalle.getCantidad()); 
			fac.getDetalles().add(detalle);
			facturaRepository.save(fac);
			detalle.setFactura(fac.getId());
			detalleRepository.save(detalle);
		}
		cliente.getFacturas().add(fac);
		clienteRepository.save(cliente);
		return new FacturaDTO(fac);
	}

}
