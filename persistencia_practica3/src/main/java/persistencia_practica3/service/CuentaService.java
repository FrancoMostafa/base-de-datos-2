package persistencia_practica3.service;

import persistencia_practica3.dto.CuentaDTO;
import persistencia_practica3.model.CuentaClienteRequest;

public interface CuentaService {
	
	public CuentaDTO create(CuentaDTO cuentaDTO);

	public CuentaDTO setCliente(CuentaClienteRequest cuentaClienteRequest);
}
