package persistencia_practica3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistencia_practica3.model.Proveedor;
import persistencia_practica3.repository.ProveedorRepository;

@Service
public class ProveedorServiceImpl implements ProveedorService {
	
	@Autowired
	private ProveedorRepository proveedorRepository;

	@Override
	public Proveedor create(Proveedor proveedorData) {
		Proveedor proveedor = new Proveedor(
				proveedorData.getDescripcion());
		proveedorRepository.save(proveedor);
		return proveedor;
	}

}
