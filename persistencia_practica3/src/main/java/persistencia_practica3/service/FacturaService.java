package persistencia_practica3.service;

import persistencia_practica3.dto.FacturaDTO;

public interface FacturaService {
	
	public FacturaDTO create(FacturaDTO facturaDTO);

}
