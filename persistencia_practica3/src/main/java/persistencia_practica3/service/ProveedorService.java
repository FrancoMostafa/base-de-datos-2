package persistencia_practica3.service;

import persistencia_practica3.model.Proveedor;

public interface ProveedorService {
	
	public Proveedor create(Proveedor proveedor);

}
