package persistencia_practica3.service;

import persistencia_practica3.model.Precio;

public interface PrecioService {
	
	public Precio create(Precio precio);

}
