package persistencia_practica3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistencia_practica3.dto.CuentaDTO;
import persistencia_practica3.model.Cliente;
import persistencia_practica3.model.Cuenta;
import persistencia_practica3.model.CuentaClienteRequest;
import persistencia_practica3.repository.ClienteRepository;
import persistencia_practica3.repository.CuentaRepository;

@Service
public class CuentaServiceImpl implements CuentaService {

	//atributos
	@Autowired
	private CuentaRepository cuentaRepository;
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public CuentaDTO create(CuentaDTO cuentaDTO) {
		Cuenta cuenta = new Cuenta(
				cuentaDTO.getId(),
				cuentaDTO.getNumero());
		cuentaRepository.save(cuenta);
		return new CuentaDTO(cuenta);
	}

	@Override
	public CuentaDTO setCliente(CuentaClienteRequest cuentaClienteRequest) {
		var clientes = (List<Cliente>) this.clienteRepository.findAll();
		var cliente = clientes.stream().filter(c -> c.getId().equals(cuentaClienteRequest.getClienteId())).findAny().orElse(null);
		var cuentas = (List<Cuenta>) this.cuentaRepository.findAll();
		var cuenta = cuentas.stream().filter(c -> c.getId().equals(cuentaClienteRequest.getCuentaId())).findAny().orElse(null);
		if(cuenta == null || cliente == null) {
			return null;
		}
		cliente.setCuenta(cuenta);
		cuenta.setCliente(cliente);
		this.cuentaRepository.save(cuenta);
		this.clienteRepository.save(cliente);
		return new CuentaDTO(cuenta);
	}
	
}
	
