package persistencia_practica3.service;

import java.util.List;

import persistencia_practica3.dto.ClienteDTO;
import persistencia_practica3.model.CuentaClienteRequest;

public interface ClienteService {
	
	public ClienteDTO create(ClienteDTO clienteDTO);

	public ClienteDTO finById(Long idCliente);

	public List<ClienteDTO> finAll();

	public ClienteDTO update(ClienteDTO clienteDTO, Long id);
	
	public List<ClienteDTO> delete(Long idCliente);
	
	public ClienteDTO setCuenta(CuentaClienteRequest cuentaClienteRequest);

}
