package persistencia_practica3.service;

import persistencia_practica3.model.General;
import persistencia_practica3.model.Producto;

public interface ProductoService {
	
	public Producto createGeneral(General generalData);
	
	public Producto agregarProveedor(Long idProducto, Long idProveedor);

}
