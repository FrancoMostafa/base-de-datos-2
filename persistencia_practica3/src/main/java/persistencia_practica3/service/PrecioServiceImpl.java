package persistencia_practica3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistencia_practica3.model.Precio;
import persistencia_practica3.repository.PrecioRepository;

@Service
public class PrecioServiceImpl implements PrecioService {
	
	@Autowired
	PrecioRepository precioRepository;

	@Override
	public Precio create(Precio precio) {
		Precio newPrecio = new Precio(
				precio.getMonto(), precio.getFecha());
		this.precioRepository.save(newPrecio);
		return newPrecio;
	}

}
