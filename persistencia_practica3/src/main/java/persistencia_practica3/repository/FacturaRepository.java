package persistencia_practica3.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import persistencia_practica3.model.Factura;

@Repository
public interface FacturaRepository extends CrudRepository<Factura, Long> {

}
