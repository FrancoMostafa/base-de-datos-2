package persistencia_practica3.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import persistencia_practica3.model.Cuenta;

@Repository
public interface CuentaRepository extends CrudRepository<Cuenta, Long> {

}
