package persistencia_practica3.dto;

import persistencia_practica3.model.Cliente;

public class ClienteDTO {

	private Long id;
	private String nombre;
	private String apellido;
	private String codigo;
	private Long cuenta;
	
	public ClienteDTO(Long id, String codigo, String apellido, String nombre) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.apellido = apellido;
		this.nombre = nombre;
	}
	
	public ClienteDTO(String codigo, String apellido, String nombre) {
		super();
		this.codigo = codigo;
		this.apellido = apellido;
		this.nombre = nombre;
	}

	public ClienteDTO(Cliente newCliente) {
			this(newCliente.getId(),
				newCliente.getCodigo(),
				 newCliente.getApellido(),
				 newCliente.getNombre()
				 );
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Long getCuenta() {
		return cuenta;
	}

	public void setCuenta(Long cuenta) {
		this.cuenta = cuenta;
	}
	
}
