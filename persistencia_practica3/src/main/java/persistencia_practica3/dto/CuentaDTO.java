package persistencia_practica3.dto;

import persistencia_practica3.model.Cuenta;

public class CuentaDTO {
	
	private Long id;
	private String numero;
	private Long cliente;
	
	public CuentaDTO(Long id, String numero) {
		super();
		this.id = id;
		this.numero = numero;
	}
	
	public CuentaDTO(String numero) {
		super();
		this.numero = numero;
	}


	public CuentaDTO(Cuenta newCuenta) {
			this(
					newCuenta.getId(),
					newCuenta.getNumero()
				 );
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Long getCliente() {
		return cliente;
	}

	public void setCliente(Long cliente) {
		this.cliente = cliente;
	}

}
