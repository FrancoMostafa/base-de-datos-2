package persistencia_practica3.dto;

import persistencia_practica3.model.Detalle;

public class DetalleDTO {
	
	private Long idProducto;
	private Detalle detalle;
	private Integer cantidad;

	public DetalleDTO(Long idProducto, Integer cantidad) {
		super();
		this.idProducto = idProducto;
		this.cantidad = cantidad;
		setDetalle(new Detalle(cantidad));
	}
	
	public DetalleDTO(Detalle detalle) {
			this(detalle.getId(),
				 detalle.getCantidad());
			setDetalle(detalle);
	}

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public Detalle getDetalle() {
		return detalle;
	}

	public void setDetalle(Detalle detalle) {
		this.detalle = detalle;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
}
