package persistencia_practica3.dto;

import java.util.ArrayList;
import java.util.List;

import persistencia_practica3.model.Factura;

public class FacturaDTO {

	private Long id;
	private Long idCliente;
	private String fecha;
	private String numero;
	private List<DetalleDTO> detalles = new ArrayList<DetalleDTO>();
	private Integer cantidad;
	
	public FacturaDTO(Long id, Long idCliente, String fecha, String numero, List<DetalleDTO> detallesDTO) {
		super();
		this.id = id;
		this.idCliente = idCliente;
		this.setFecha(fecha);
		this.setFecha(fecha);
		this.numero = numero;
		this.detalles = detallesDTO;
	}
	
	public FacturaDTO(Long idCliente, String fecha, String numero, List<DetalleDTO> detallesDTO) {
		super();
		this.idCliente = idCliente;
		this.setFecha(fecha);
		this.setFecha(fecha);
		this.numero = numero;
		this.detalles = detallesDTO;
	}
	
	public FacturaDTO(Factura factura) 
		{
			this(factura.getId(),
				factura.getCliente(),
				 factura.getFecha(),
				 factura.getNumero(),
				 new ArrayList<DetalleDTO>()
				 );
			List<DetalleDTO> detallesDTO = new ArrayList<DetalleDTO>();
			factura.getDetalles().forEach(d -> detallesDTO.add(new DetalleDTO(d)));
			this.setDetalles(detallesDTO);
	}
	
	public FacturaDTO() {
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public List<DetalleDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<DetalleDTO> detalles) {
		this.detalles = detalles;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
}
