package persistencia_practica3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersistenciaPractica3Application {

	public static void main(String[] args) {
		SpringApplication.run(PersistenciaPractica3Application.class, args);
	}

}
