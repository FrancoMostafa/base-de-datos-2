package springbootpersistencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPersistenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPersistenciaApplication.class, args);
	}

}
