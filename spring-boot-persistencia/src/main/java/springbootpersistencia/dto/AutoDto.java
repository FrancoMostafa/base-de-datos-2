package springbootpersistencia.dto;

import springbootpersistencia.model.Auto;

public class AutoDto {

	private Long id;
	private String codigo;
	private String marca;
	private Integer anioDeFabricacion;
	
	public AutoDto(Long id, String codigo, String marca, Integer anioDeFabricacion) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.marca = marca;
		this.anioDeFabricacion = anioDeFabricacion;
	}

	public AutoDto(Auto newAuto) {
			this(newAuto.getId(),
				newAuto.getCodigo(),
				 newAuto.getMarca(),
				 newAuto.getAnioDeFabricacion()
				 );
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getAnioDeFabricacion() {
		return anioDeFabricacion;
	}

	public void setAnioDeFabricacion(Integer anioDeFabricacion) {
		this.anioDeFabricacion = anioDeFabricacion;
	}
	
}
