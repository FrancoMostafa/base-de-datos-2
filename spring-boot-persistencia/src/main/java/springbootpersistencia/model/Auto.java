package springbootpersistencia.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Auto")
@Table(name = "auto")
public class Auto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	
	private String codigo;
	private String marca;
	private Integer anioDeFabricacion;
	
	public Auto(String codigo, String marca, Integer anioDeFabricacion) {
		super();
		this.setId(((Double) Math.floor(Math.random() * (1000 - 1 + 1) + 1)).longValue());
		this.codigo = codigo;
		this.marca = marca;
		this.anioDeFabricacion = anioDeFabricacion;
	}
	
	public Auto() {}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Integer getAnioDeFabricacion() {
		return anioDeFabricacion;
	}
	public void setAnioDeFabricacion(Integer anioDeFrabricacion) {
		this.anioDeFabricacion = anioDeFrabricacion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
