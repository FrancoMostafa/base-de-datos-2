package springbootpersistencia.service;

import java.util.List;

import springbootpersistencia.dto.AutoDto;
import springbootpersistencia.model.Auto;

public interface AutoService {
	

	public AutoDto crearAuto(AutoDto auto);
	
	public List<AutoDto> findAll();
	
	public AutoDto findByCodigo(String codigo);
	
	public AutoDto findById(Long id);
	
	public AutoDto update(Auto newData, Long id);
	
	public AutoDto delete(Long id);

}
