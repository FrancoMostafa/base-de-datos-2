package springbootpersistencia.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springbootpersistencia.dto.AutoDto;
import springbootpersistencia.model.Auto;
import springbootpersistencia.repository.AutoRepository;

@Service
public class AutoServiceImp implements AutoService {
	
	@Autowired
	private AutoRepository autoRepository;

	@Override
	public AutoDto crearAuto(AutoDto autoDto) {
		Auto newAuto = new Auto(
				autoDto.getCodigo(),
				autoDto.getMarca(),
				autoDto.getAnioDeFabricacion());
		this.autoRepository.save(newAuto);
		return new AutoDto(newAuto);
	}

	@Override
	public List<AutoDto> findAll() {
		var autos = (List<Auto>) this.autoRepository.findAll();
		var autosDto = new ArrayList<AutoDto>();
		if(autos.size() == 0) {
			return autosDto;
		}
		autos.forEach(a -> autosDto.add(new AutoDto(a)));
		return autosDto;
	}

	@Override
	public AutoDto findByCodigo(String codigo) {
		var autos = (List<Auto>) this.autoRepository.findAll();
		var auto = autos.stream().filter(a -> a.getCodigo().equals(codigo)).findAny().orElse(null);
		if(auto == null) {
			return null;
		}
		return new AutoDto(auto);
	}
	
	@Override
	public AutoDto findById(Long id) {
		var autos = (List<Auto>) this.autoRepository.findAll();
		var auto = autos.stream().filter(a -> a.getId().equals(id)).findAny().orElse(null);
		if(auto == null) {
			return null;
		}
		return new AutoDto(auto);
	}

	@Override
	public AutoDto update(Auto newData, Long id) {
		var autos = (List<Auto>) this.autoRepository.findAll();
		var auto = autos.stream().filter(a -> a.getId().equals(id)).findAny().orElse(null);
		if(auto == null) {
			return null;
		}
		if(newData.getCodigo() != null) {
			auto.setCodigo(newData.getCodigo());
		}
		if(newData.getMarca() != null) {
			auto.setMarca(newData.getMarca());
		}
		if(newData.getAnioDeFabricacion() != null) {
			auto.setAnioDeFabricacion(newData.getAnioDeFabricacion());
		}
		this.autoRepository.save(auto);
		return new AutoDto(auto);
	}

	@Override
	public AutoDto delete(Long id) {
		var autos = (List<Auto>) this.autoRepository.findAll();
		var auto = autos.stream().filter(a -> a.getId().equals(id)).findAny().orElse(null);
		if(auto == null) {
			return null;
		}
		this.autoRepository.delete(auto);
		return new AutoDto(auto);
	}

}
