package springbootpersistencia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import springbootpersistencia.dto.AutoDto;
import springbootpersistencia.model.Auto;
import springbootpersistencia.service.AutoServiceImp;

@RestController @RequestMapping("/api")
public class AutoRestController {
	
	@Autowired
	private AutoServiceImp autoService;
	
	public AutoRestController(List<Auto> autos) {
		super();
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<AutoDto> crearAuto(@RequestBody AutoDto nuevo) {
		var auto = autoService.crearAuto(nuevo);
		return new ResponseEntity<AutoDto>(auto, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/findAll", method=RequestMethod.GET)
	public ResponseEntity<List<AutoDto>> findAll() {
		var autos = autoService.findAll();
		return new ResponseEntity<List<AutoDto>>(autos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/codigo/{codigo}", method=RequestMethod.GET)
	public ResponseEntity<AutoDto> findByCodigo(@PathVariable("codigo") String codigo) {
		var auto = autoService.findByCodigo(codigo);
		return new ResponseEntity<AutoDto>(auto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<AutoDto> findById(@PathVariable("id") Long id) {
		var auto = autoService.findById(id);
		return new ResponseEntity<AutoDto>(auto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<AutoDto> deleteAuto(@PathVariable("id") Long id) {
		var auto = autoService.delete(id);
		return new ResponseEntity<AutoDto>(auto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<AutoDto> editAuto(@RequestBody Auto data, @PathVariable("id") Long id) {
		var auto = autoService.update(data, id);
		return new ResponseEntity<AutoDto>(auto, HttpStatus.OK);
	}

}
