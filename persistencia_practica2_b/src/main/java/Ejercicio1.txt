En producto use @PrimaryKeyJoinColumn para que se renombre la clave primaria de esta clase en sus subclases.

En alimento use @Inheritance(strategy = InheritanceType.JOINED) para que en la tabla se muestre los datos de las subclases.