package persistencia_practica2;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import persistencia_practica2.model.Alimento;
import persistencia_practica2.model.Congelado;
import persistencia_practica2.model.Frio;
import persistencia_practica2.model.General;
import persistencia_practica2.model.Gondola;
import persistencia_practica2.model.Precio;
import persistencia_practica2.model.Producto;
import persistencia_practica2.model.Proveedor;

public class Main {

	public static void main(String[] args) {
	    SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		List<Proveedor> proveedores = new ArrayList<Proveedor>();
		
		List<Proveedor> proveedores2 = new ArrayList<Proveedor>();
		
		Proveedor proveedor = crearProveedor("Proveedor1");
		
		Proveedor proveedor2 = crearProveedor("Proveedor2");
		
		Proveedor proveedor3 = crearProveedor("Proveedor3");
		
		Precio precio = crearPrecio(new BigDecimal("350.00"), LocalDate.now());
		
		Precio precio2 = crearPrecio(new BigDecimal("700.00"), LocalDate.now());

		Precio precio3 = crearPrecio(new BigDecimal("1650.00"), LocalDate.now());
		
		Precio precio4 = crearPrecio(new BigDecimal("5000.00"), LocalDate.now());
		
		Precio precio5 = crearPrecio(new BigDecimal("1500.00"), LocalDate.now());
		
		proveedores.add(proveedor);

		proveedores2.add(proveedor2);
		
		proveedores2.add(proveedor3);
		
		General general = crearGeneral("ABC1", "Leche", precio, proveedores2, 50);
		
		General general2 = crearGeneral("ABC2", "Computadora", precio5, proveedores2, 600);
		
		Congelado congelado = crearCongelado("ABC3", "Pescado", precio2, proveedores2, LocalDate.now());
		
		Frio frio = crearFrio("ABC4", "Helado", precio3, proveedores, LocalDate.now(), 5, -15);
		
		Gondola gondola = crearGondola("ABC5", "Gondola", precio4, proveedores, LocalDate.now(), 500);
				
		try {
			session.beginTransaction();
			session.persist(proveedor);
			session.persist(proveedor2);
			session.persist(proveedor3);
			session.persist(precio);
			session.persist(precio2);
			session.persist(precio3);
			session.persist(precio4);
			session.persist(precio5);
			session.persist(general);
			session.persist(general2);
			session.persist(congelado);
			session.persist(frio);
			session.persist(gondola);
			session.getTransaction().commit();
			Query queryTodosLosProductos = session.createQuery("from Producto");
			Query queryTodosLosAlimentos = session.createQuery("from Alimento");
			Query queryTodasLasGondolas = session.createQuery("from Gondola");
			Query queryGeneralesConPesoMayorA400 = session.createQuery("from General where peso > 400");
			@SuppressWarnings("unchecked")
			List<Producto> todosLosProductos = queryTodosLosProductos.getResultList();
			@SuppressWarnings("unchecked")
			List<Alimento> todosLosAlimentos = queryTodosLosAlimentos.getResultList();
			@SuppressWarnings("unchecked")
			List<Gondola> todasLasGondolas = queryTodasLasGondolas.getResultList();
			@SuppressWarnings("unchecked")
			List<General> generalesConPesoMayorA400 = queryGeneralesConPesoMayorA400.getResultList();
			List<General> generalesFiltrados = generalesConPesoMayorA400.stream().filter(g -> g.getPrecioFinal().compareTo(new BigDecimal("1500.00")) == 1).collect(Collectors.toList());
			System.out.println("TODOS LOS PRODUCTOS");
			todosLosProductos.forEach(a -> System.out.println(a.toString()));
			System.out.println("TODOS LOS ALIMENTOS");
			todosLosAlimentos.forEach(a -> System.out.println(a.toString()));
			System.out.println("TODAS LAS GONDOLAS");
			todasLasGondolas.forEach(a -> System.out.println(a.toString()));
			System.out.println("GENERAL CON UN PESO MAYOR A 400 Y PRECIO FINAL MAYOR A $1500");
			generalesFiltrados.forEach(a -> System.out.println(a.toString()));
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		session.close();

	}

	
	public static Proveedor crearProveedor(String descripcion) {
		return new Proveedor(descripcion);
	}
	
	public static Congelado crearCongelado(String codigo, String descripcion, Precio precio, List<Proveedor> proveedor, LocalDate fechaDeIngreso) {
		return new Congelado(codigo,descripcion, precio, proveedor, fechaDeIngreso);
	}
	
	public static Frio crearFrio(String codigo, String descripcion, Precio precio, List<Proveedor> proveedor, LocalDate fechaDeIngreso, Integer temperaturaMaxima, Integer temperaturaMinima) {
		return new Frio(codigo,descripcion, precio, proveedor, fechaDeIngreso, temperaturaMaxima, temperaturaMinima);
	}
	
	public static Gondola crearGondola(String codigo, String descripcion, Precio precio, List<Proveedor> proveedor, LocalDate fechaDeIngreso, Integer volumen) {
		return new Gondola(codigo,descripcion, precio, proveedor, fechaDeIngreso, volumen);
	}
	
	public static General crearGeneral(String codigo, String descripcion, Precio precio, List<Proveedor> proveedor, Integer peso) {
		return new General(codigo,descripcion, precio, proveedor, peso);
	}
	
	public static Precio crearPrecio(BigDecimal monto, LocalDate fecha) {
		return new Precio(monto, fecha);
	}


}
