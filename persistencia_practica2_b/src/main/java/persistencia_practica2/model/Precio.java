package persistencia_practica2.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "Precio")
@Table(name = "precio")
public class Precio {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos",strategy="native")
	private Long id;
	
	private BigDecimal monto;
	private LocalDate fecha;
	
	@OneToOne(mappedBy="precio", cascade=CascadeType.ALL)
	private Producto producto;
	
	public Precio(BigDecimal monto, LocalDate localDate) {
		super();
		this.monto = monto;
		this.fecha = localDate;
	}
	
	public Precio() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {return true;}
		if(!(obj instanceof Precio)) {
				return false;
		}
		Precio otherObj = (Precio) obj;
		return this.getId() == otherObj.getId();
	}

	@Override
	public String toString() {
		return "Precio [id=" + id + ", monto=" + monto + ", fecha=" + fecha + ", producto=" + producto + "]";
	}
	
}
