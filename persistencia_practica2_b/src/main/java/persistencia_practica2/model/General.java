package persistencia_practica2.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="General")
@PrimaryKeyJoinColumn(name = "general_id")
public class General extends Producto {
	
	private Integer peso;
	
	public General(String codigo, String descripcion, Precio precio, List<Proveedor> proveedores,Integer peso) {
		super(codigo, descripcion, precio, proveedores);
		this.peso = peso;
	}

	@Override
	public BigDecimal getPrecioFinal() {
		var incremento = new BigDecimal("0.00");
		if(this.getPeso() > 2 && this.getPeso() < 4) {
			incremento = this.getPrecio().getMonto().multiply(new BigDecimal("0.04"));
		}
		else if(this.getPeso() > 4 && this.getPeso() < 7)  {
			incremento = this.getPrecio().getMonto().multiply(new BigDecimal("0.07"));
		}
		else if(this.getPeso() > 7) {
			incremento = this.getPrecio().getMonto().multiply(new BigDecimal("0.12"));
		}
		else {
			incremento = new BigDecimal("0.00");
		}
		return this.getPrecio().getMonto().add(incremento);
	}
	
	public Integer getPeso() {
		return peso;
	}

	@Override
	public String toString() {
		return "General [peso=" + peso + ", id=" + id + ", codigo=" + codigo + ", descripcion=" + descripcion
				+ ", precio=" + precio + ", precioHistorico=" + precioHistorico + ", proveedores=" + proveedores + "]";
	}

}
