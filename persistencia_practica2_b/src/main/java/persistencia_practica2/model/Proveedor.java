package persistencia_practica2.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "Proveedor")
@Table(name = "proveedor")
public class Proveedor {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="nativoDeBaseDeDatos")
	@GenericGenerator(name="nativoDeBaseDeDatos",strategy="native")
	private Long id;
	private String descripcion;
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Producto> productos;
	
	public Proveedor(String descripcion) {
		super();
		this.descripcion = descripcion;
	}
	
	public Proveedor() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {return true;}
		if(!(obj instanceof Proveedor)) {
				return false;
		}
		Proveedor otherObj = (Proveedor) obj;
		return this.getId() == otherObj.getId();
	}

	@Override
	public String toString() {
		return "Proveedor [id=" + id + ", descripcion=" + descripcion + ", productos=" + productos + "]";
	}
	
}
