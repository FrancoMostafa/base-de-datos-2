package persistencia_practica2.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="Frio")
@PrimaryKeyJoinColumn(name = "frio_id")
public class Frio extends Alimento {
	
	private Integer temperaturaMaxima;
	private Integer temperaturaMinima;

	public Frio(String codigo, String descripcion, Precio precio, List<Proveedor> proveedores, LocalDate fechaDeIngreso,
			Integer temperaturaMaxima, Integer temperaturaMinima) {
		super(codigo, descripcion, precio, proveedores, fechaDeIngreso);
		this.temperaturaMaxima = temperaturaMaxima;
		this.temperaturaMinima = temperaturaMinima;
	}

	@Override
	public BigDecimal getPrecioFinal() {
		var precioPorTraslado = this.getPrecio().getMonto().add(this.getPrecio().getMonto().multiply(new BigDecimal("0.05")));
		if(this.tieneMasDe5DiasEnElLocal()) {
			return this.getPrecio().getMonto().add(precioPorTraslado).divide(new BigDecimal("2.00"));
		}
		else {
			return this.getPrecio().getMonto().add(precioPorTraslado);
		}
	}

	public Integer getTemperaturaMaxima() {
		return temperaturaMaxima;
	}

	public void setTemperaturaMaxima(Integer temperaturaMaxima) {
		this.temperaturaMaxima = temperaturaMaxima;
	}

	public Integer getTemperaturaMinima() {
		return temperaturaMinima;
	}

	public void setTemperaturaMinima(Integer temperaturaMinima) {
		this.temperaturaMinima = temperaturaMinima;
	}
	
	private boolean tieneMasDe5DiasEnElLocal() {
		var fechaDeHoy = LocalDate.now();
		return Math.abs(ChronoUnit.DAYS.between(fechaDeHoy, this.getFechaDeIngreso())) + 1 >= 5;
	}

	@Override
	public String toString() {
		return "Frio [temperaturaMaxima=" + temperaturaMaxima + ", temperaturaMinima=" + temperaturaMinima
				+ ", fechaDeIngreso=" + fechaDeIngreso + ", id=" + id + ", codigo=" + codigo + ", descripcion="
				+ descripcion + ", precio=" + precio + ", precioHistorico=" + precioHistorico + ", proveedores="
				+ proveedores + "]";
	}

}
